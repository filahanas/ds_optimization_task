
public class ShortImpl implements LongArray {

    private short[] outArray;

    public ShortImpl(long[] inArray) {
    outArray=new short[inArray.length];
       for(int i=0;i<inArray.length;i++) this.outArray[i]=(short)inArray[i];
    }

    @Override
    public long get(int index) {
        long l = outArray[index];
        return l;
    }

    public int getsize(){
        return outArray.length;
    }
}

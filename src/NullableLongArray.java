public interface NullableLongArray {
    boolean isNull(int index);
    long get(int index);
    Long getNullable(int index);
}

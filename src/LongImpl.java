public class LongImpl implements LongArray {

    private long[] outArray;

    public LongImpl(long[] inArray) {
        outArray=new long[inArray.length];
    this.outArray=inArray;
    }

    @Override
    public long get(int index) {
        long l = outArray[index];
        return l;
    }

    public int getsize(){
        return outArray.length;
    }
}

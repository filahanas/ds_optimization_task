public class TestMain {
    public static void main(String[] args) {


        //Ex 1
        long[] tab1={10,10,10};
        LongArray ntab1=Factory.reduceTypeStrength(tab1);
        System.out.println("EX1: ");
        System.out.println(ntab1.get(0));
        System.out. println(" "+ntab1.getClass().getName());//retourne ByteImpl

        long[] tab2={1000,10,10};
        LongArray ntab2=Factory.reduceTypeStrength(tab2);
        System.out.println(ntab2.get(0));
        System.out.println(" "+ntab2.getClass().getName());//retourne ShortImpl

        long[] tab3={100000,10,10};
        LongArray ntab3=Factory.reduceTypeStrength(tab3);
        System.out.println(ntab3.get(0));
        System.out.println(" "+ntab3.getClass().getName());//retourne IntegerImpl

        long[] tab4={1000000000000L,10,10};
        LongArray ntab4=Factory.reduceTypeStrength(tab4);
        System.out.println(ntab4.get(0));
        System.out.println(" "+ntab4.getClass().getName());//retourne LongImplImpl


        //Ex 2
        LongArray monotonicarrays=new Monotonicarrays(new long[]{1,100,1000000,100000000000L});
        System.out.println("\n\nEX2: "+monotonicarrays.get(2));
        LongArray monotonicarrays2=new Monotonicarrays(new long[]{100000000000L,1000000,100,1});
        System.out.println(monotonicarrays2.get(0));



        //Ex 3
        LongArray longarray =new RepeatingvalArrays(new long[]{1,2,2,2,3,3,5,0});
        System.out.println("\n\nEX3: "+longarray.get(2));


        //Ex 4
        NullableLongArray tab=new NullableLongArrayImpl(new Long[]{1L,2L,null,5L});
        System.out.println("\n\nEX4: "+"the element 2 is null:"+tab.isNull(2));
        System.out.println(tab.get(3));
        System.out.println(tab.getNullable(2));


}


    }

import java.util.ArrayList;
import java.util.List;

//for this implementation, if we have a table with different values, using this implementation we will make the size of the array bigger and consume more memory
//ex new long[]{1,2,5,12,5,14,9,0}=>new long[]{1,1,2,1,5,1,12,1,5,1,14,1,9,1,0,1}

public class RepeatingvalArrays implements LongArray {
    private long[] inArray;
    private long[] outArray;

    public RepeatingvalArrays(long[] inArray) {
        this.inArray = inArray;
        int count=1;
        List<Long> mylist= new ArrayList<Long>();
        for(int i=0;i<inArray.length;i++){
            if(i==inArray.length-1){
                mylist.add(inArray[i]);
                mylist.add((long)count);
            }
            else if(inArray[i]==inArray[i+1]){
               count++;
            }
         else{
                mylist.add(inArray[i]);
                mylist.add((long)count);
             count=1;
         }

        }

        outArray=new long[mylist.size()];
       for(int j=0;j<mylist.size();j++)
           outArray[j]=mylist.get(j);

    }


    @Override
    public long get(int index) {
        long avant=0;
        long apres=0;
       for(int i=1;i<outArray.length;i=i+2){
           avant=apres;
           apres=apres+outArray[i];
           if(index>=avant && index<apres) return outArray[i-1];
       }
        return 0;
    }
}

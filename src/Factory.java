

public class Factory {

   static public int numberofDigits(long number) {
        int count = 1;
        while (number / 2 != 0) {
            number /= 2;
            count++;
        }
        return count;
    }

   static public LongArray reduceTypeStrength(long[] inArray) {
        boolean shortnbr = false,integernbr = false;
        for (int i = 0; i < inArray.length; i++) {
            if (numberofDigits(inArray[i]) >32) return new LongImpl(inArray);

            if (numberofDigits(inArray[i]) > 16) integernbr=true;


            if (numberofDigits(inArray[i]) > 8) shortnbr=true;

        }


        if(integernbr) return new IntegerImpl(inArray);
        if(shortnbr) return new ShortImpl(inArray);
        else return new ByteImpl(inArray);



    }
}
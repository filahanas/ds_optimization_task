import java.util.ArrayList;
import java.util.List;

/*
-This implementation stores an input array into multiple arrays depending on the type of the data
-We will use threads to save time, and to have the same time of response of the method "get" with every index giving.
*/

public class Monotonicarrays implements LongArray {

    private ByteImpl byteimpl;
    private ShortImpl shortimpl;
    private IntegerImpl integerimpl;
    private LongImpl longimpl;
    private boolean croissante;

    public Monotonicarrays(long[] inArray) {

        List<Byte> listByte = new ArrayList<Byte> ();
        List<Short> listShort = new ArrayList<Short> ();
        List<Integer> listInteger = new ArrayList<Integer> ();
        List<Long> listLong= new ArrayList<Long> ();

        if(inArray[0]<=inArray[inArray.length-1]) croissante=true;
        else croissante=false;
        int i=0;

        while (i<inArray.length) {
    int j = 0;
    while (i < inArray.length && Factory.numberofDigits(inArray[i]) <= 8) {
        listByte.add(j, (byte) inArray[i]);
        i++;
        j++;
    }
    j = 0;
    while (i < inArray.length && Factory.numberofDigits(inArray[i]) > 8 && Factory.numberofDigits(inArray[i]) <= 16) {
        listShort.add(j, (short) inArray[i]);
        i++;
        j++;
    }

    j = 0;
    while (i < inArray.length && Factory.numberofDigits(inArray[i]) > 16 && Factory.numberofDigits(inArray[i]) <= 32) {
        listInteger.add(j, (int) inArray[i]);
        i++;
        j++;

    }
    j = 0;
    while (i < inArray.length && Factory.numberofDigits(inArray[i]) > 32 && Factory.numberofDigits(inArray[i]) <= 64) {
        listLong.add(j, inArray[i]);
        i++;
        j++;
    }
}


            long [] tabbyte=new long[listByte.size()];
        long [] tabshort=new long[listShort.size()];
        long [] tabinteger=new long[listInteger.size()];
        long [] tablong=new long[listLong.size()];

        for (int k=0;k<listByte.size();k++){
          tabbyte[k]=listByte.get(k);
        }
        for (int k=0;i<listShort.size();k++){
            tabshort[k]=listShort.get(k);
        }
        for (int k=0;k<listInteger.size();k++){
            tabinteger[k]=listInteger.get(k);
        }
        for (int k=0;k<listLong.size();k++){
            tablong[k]=listLong.get(k);
        }

        this.byteimpl=new ByteImpl(tabbyte);
        this.shortimpl=new ShortImpl(tabshort);
        this.integerimpl=new IntegerImpl(tabinteger);
        this.longimpl=new LongImpl(tablong);

    }

    @Override
    public long get(int index) {


        final long[] resultat = new long[1];

        if(croissante) {
            Thread t1 = new Thread() {
                public void run() {
                    if (index < byteimpl.getsize())
                        resultat[0] =byteimpl.get(index);
                }
            };

            Thread t2 = new Thread() {
                public void run() {
                    if (byteimpl.getsize() - 1 < index && index < byteimpl.getsize() + shortimpl.getsize())
                        resultat[0] = shortimpl.get(index - byteimpl.getsize());                }
            };


            Thread t3 = new Thread() {
                public void run() {
                    if (byteimpl.getsize() + shortimpl.getsize() - 1 < index && index < byteimpl.getsize() + shortimpl.getsize() + integerimpl.getsize())
                        resultat[0] = integerimpl.get(index - (byteimpl.getsize() + shortimpl.getsize()));                }
            };

            Thread t4 = new Thread() {
                public void run() {
                    if (byteimpl.getsize() + shortimpl.getsize() + integerimpl.getsize() - 1 < index && index < byteimpl.getsize() + shortimpl.getsize() + integerimpl.getsize() + longimpl.getsize())
                        resultat[0]= longimpl.get(index - (byteimpl.getsize() + shortimpl.getsize() + integerimpl.getsize()));              }
            };
            t1.start();
            t2.start();
            t3.start();
            t4.start();
        }

        else {

            Thread t1 = new Thread() {
                public void run() {
                    if (index < longimpl.getsize())
                        resultat[0]= longimpl.get(index);
                }
            };
            Thread t2 = new Thread() {
                public void run() {
                    if(longimpl.getsize()-1<index && index<longimpl.getsize()+integerimpl.getsize())
                        resultat[0]= integerimpl.get(index-longimpl.getsize());
                }
            };
            Thread t3 = new Thread() {
                public void run() {
                    if(longimpl.getsize()+integerimpl.getsize()-1<index && index<longimpl.getsize()+integerimpl.getsize()+shortimpl.getsize())
                        resultat[0]= shortimpl.get(index-(longimpl.getsize()+integerimpl.getsize()));

                }
            };
            Thread t4 = new Thread() {
                public void run() {
                    if(longimpl.getsize()+integerimpl.getsize()+shortimpl.getsize()-1<index && index<longimpl.getsize()+integerimpl.getsize()+shortimpl.getsize()+byteimpl.getsize())
                        resultat[0]= byteimpl.get(index-(longimpl.getsize()+integerimpl.getsize()+shortimpl.getsize()));
                }
            };
            t1.start();
            t2.start();
            t3.start();
            t4.start();
        }

   return resultat[0];
    }





}

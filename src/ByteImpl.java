public class ByteImpl implements LongArray {

    private byte[] outArray;

    public ByteImpl(long[] inArray) {
        outArray=new byte[inArray.length];
        for(int i=0;i<inArray.length;i++) this.outArray[i]=(byte)inArray[i];

    }

    @Override
    public long get(int index) {
        long l = outArray[index];
        return l;
    }

    public int getsize(){
        return outArray.length;
    }
}

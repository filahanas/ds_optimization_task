import java.util.ArrayList;
import java.util.List;

public class NullableLongArrayImpl implements NullableLongArray {

    long[] nullArray;
    long[] notnullArray;


    public NullableLongArrayImpl(Long [] Array){


        List<Long> nullist=new ArrayList<>();
        List<Long> notnulllist=new ArrayList<>();
        for(int i=0;i<Array.length;i++){
        if(Array[i]==null)nullist.add((long) i);
        else notnulllist.add(Array[i]);
        }

        nullArray=new long[nullist.size()];
        for(int i=0;i<nullist.size();i++) nullArray[i]=nullist.get(i);

        notnullArray=new long[notnulllist.size()];
        for(int i=0;i<notnulllist.size();i++) notnullArray[i]=notnulllist.get(i);


    }
    @Override
    public boolean isNull(int index) {
        for(int i=0;i<nullArray.length;i++){
            if(index==nullArray[i])return true;
        }
        return false;
    }

    @Override
    public long get(int index) {
        int nullindex=0;
        try {
            if (isNull(index)) throw new Exception();
            for(int i=0;i<nullArray.length;i++){
                if(index>nullArray[i]){
                    nullindex=nullindex+1;
                }
            }
        }catch (Exception e){
            System.out.println("the variable is null");
        }

        return notnullArray[index-nullindex];

    }

    @Override
    public Long getNullable(int index) {
        if(isNull(index))return null;
        else return get(index);
    }
}

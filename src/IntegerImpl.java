import static java.lang.System.in;

public class IntegerImpl implements LongArray {

    private int[] outArray;

    public IntegerImpl(long[] inArray) {
        outArray=new int[inArray.length];
        for(int i=0;i<inArray.length;i++) this.outArray[i]=(int)inArray[i];
    }

    @Override
    public long get(int index) {
        long l = outArray[index];
        return l;
    }

    public int getsize(){
        return outArray.length;
    }
}
;